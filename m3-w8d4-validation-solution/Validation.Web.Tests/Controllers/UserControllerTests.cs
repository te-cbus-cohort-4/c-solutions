﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Validation.Web.Controllers;
using Validation.Web.Models;

namespace Validation.Web.Tests.Controllers
{
    [TestClass]
    public class UserControllerTests
    {
        // Check to see if Index Action Returns Index View
        [TestMethod]
        public void IndexAction_ReturnsIndexView()
        {
            var controller = new UsersController();

            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
        }

        // Check to see if GET Login Returns Login View
        [TestMethod]
        public void LoginAction_HttpGet_ReturnsLoginView()
        {
            var controller = new UsersController();

            var result = controller.Login() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Login", result.ViewName);
        }

        // Check to see if POST Login Returns Login View (with Validation Errors)
        [TestMethod]
        public void LoginAction_WithErrors_HttpPost_ReturnsLoginView()
        {
            var controller = new UsersController();
            var model = new LoginViewModel();

            // Fake an error
            controller.ModelState.AddModelError("test", "test error");
            var result = controller.Login(model) as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Login", result.ViewName);
        }


        // Check to see if POST Login Returns Redirect Confirmation Action (when successful)
        [TestMethod]
        public void LoginAction_HttpPost_ReturnsRedirect()
        {
            var controller = new UsersController();
            var model = new LoginViewModel();

            var result = controller.Login(model) as RedirectToRouteResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Users", result.RouteValues["controller"]);
            Assert.AreEqual("Confirmation", result.RouteValues["action"]);
        }

        // Check to see if GET Registration Returns Registration View
        [TestMethod]
        public void RegistrationAction_HttpGet_ReturnsRegisterView()
        {
            var controller = new UsersController();

            var result = controller.Register() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Register", result.ViewName);
        }

        // Check to see if POST Registration Returns Registration View (with Validation Errors)
        // Check to see if POST Login Returns Login View (with Validation Errors)
        [TestMethod]
        public void RegisterAction_WithErrors_HttpPost_ReturnsRegisterView()
        {
            var controller = new UsersController();
            var model = new RegistrationViewModel();

            // Fake an error
            controller.ModelState.AddModelError("test", "test error");
            var result = controller.Register(model) as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Register", result.ViewName);
        }

        // Check to see if POST Registration Returns Confirmation Action (when successful)
        [TestMethod]
        public void RegistrationAction_HttpPost_ReturnsRedirect()
        {
            var controller = new UsersController();
            var model = new RegistrationViewModel();

            var result = controller.Register(model) as RedirectToRouteResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Users", result.RouteValues["controller"]);
            Assert.AreEqual("Confirmation", result.RouteValues["action"]);
        }

    }
}
