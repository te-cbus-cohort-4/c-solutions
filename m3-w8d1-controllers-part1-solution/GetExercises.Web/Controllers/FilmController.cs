﻿using GetExercises.Web.DAL.Interfaces;
using GetExercises.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GetExercises.Web.Controllers
{
    public class FilmController : Controller
    {
        private IFilmDAL dal;

        /// <summary>
        /// The MVC framework with the assistance of Ninject, automatically passes in a FilmDAL.
        /// </summary>
        /// <param name="dal"></param>
        public FilmController(IFilmDAL dal)
        {
            this.dal = dal;
        }

        // GET: Film
        public ActionResult Index()
        {
            return View("Index", new FilmSearch());
        }

        public ActionResult SearchResult(FilmSearch request)
        {                        
            IList<Film> films = dal.GetFilmsBetween(request.Genre, request.MinimumLength.GetValueOrDefault(0), request.MaximumLength.GetValueOrDefault(1000));
            request.Results = films;

            return View("Index", request);
        }
    }
}